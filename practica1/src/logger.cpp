#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char* argv[]) {
	
	//Creamos la tubería
	mkfifo("/tmp/loggerfifo",0777);

	//La abrirmos en modo lectura	
	int fd=open("/tmp/loggerfifo", O_RDONLY);

	if(fd == -1)
	{
		printf ("Error abriendo fifo");
		exit (EXIT_FAILURE);
	}

	int salir=0;
	int aux;

	//El logger entra en un bucle infinito de recepción de datos
	while(salir==0)
	{
		char buff[200];

		//Leemos el mensaje
		aux=read(fd,buff,sizeof(buff));
 		
		//Y lo imprimimos por salida estándar
		printf("%s\n", buff);

		//Si se produce algun error en el read o lee menos de lo que debería leer
		if((aux==-1)||(aux=!sizeof(buff)))
		{
			printf ("Error leyendo fichero");
			exit (EXIT_FAILURE);
		}
 
		if(buff[0]=='-')//Si terminamos con la tubería.
		{

			printf("----Cerrando logger...--- \n");
			salir=1; //Salimos del bucle
		}
	}

	//La destruimos 
	close(fd);
	unlink("/tmp/loggerfifo");
	return 0;
}
